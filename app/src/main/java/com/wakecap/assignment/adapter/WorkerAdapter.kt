package com.wakecap.assignment.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wakecap.assignment.R
import com.wakecap.assignment.bean.WorkerBean

class WorkerAdapter(private val workers: ArrayList<WorkerBean>,private val context:Context) : RecyclerView.Adapter<WorkerAdapter.WorkerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkerHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.list_item_worker, parent, false)
        return WorkerAdapter.WorkerHolder(v)
    }

    override fun getItemCount(): Int {
        return workers.size
    }

    override fun onBindViewHolder(holder: WorkerHolder, position: Int) {
        holder.bindItems(workers[position],context)
    }

    class WorkerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(worker: WorkerBean,context: Context) {
            val textViewName = itemView.findViewById(R.id.textViewName) as TextView
            val textViewId  = itemView.findViewById(R.id.textViewId) as TextView
            val textViewRole  = itemView.findViewById(R.id.textViewRole) as TextView
            textViewName.text = worker.name
            textViewId.text =   context.getString(R.string.worker_id,worker.id)
            textViewRole.text = worker.role
        }
    }
}
