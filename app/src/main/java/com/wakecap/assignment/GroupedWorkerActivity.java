package com.wakecap.assignment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.wakecap.assignment.bean.WorkerBean;
import com.wakecap.assignment.fragment.WorkerListFragment;
import com.wakecap.assignment.network.ApiHelper;
import com.wakecap.assignment.network.NetworkResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupedWorkerActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog dialog;

    HashMap<String, ArrayList<WorkerBean>> mainMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grouped_worker);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Worker List (Grouped)");
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewpager);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Getting a list of workers...");

        mainMap = new HashMap<>();

        new ApiHelper(new NetworkResultListener() {
            @Override
            public void onPreExecute() {
                dialog.show();
            }

            @Override
            public void onResult(String result) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                try {
                    JSONArray workerArray = new JSONObject(result).getJSONObject("data").getJSONArray("items");
                    for (int i = 0; i < workerArray.length(); i++) {
                        JSONObject item = workerArray.getJSONObject(i).getJSONObject("attributes");

                        if (mainMap.containsKey(item.getString("role"))) {
                            ArrayList<WorkerBean> workerList = mainMap.get(item.getString("role"));
                            workerList.add(new WorkerBean(item.getInt("id"), item.getString("full_name"), item.getString("role")));
                            mainMap.put(item.getString("role"), workerList);
                        } else {
                            ArrayList<WorkerBean> workerList = new ArrayList<>();
                            workerList.add(new WorkerBean(item.getInt("id"), item.getString("full_name"), item.getString("role")));
                            mainMap.put(item.getString("role"), workerList);
                        }
                    }
                    setupViewPager(viewPager);
                    tabLayout.setupWithViewPager(viewPager);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Invalid API response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), "Invalid API response", Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (Map.Entry<String, ArrayList<WorkerBean>> entry : mainMap.entrySet()) {
            String key = entry.getKey();
            ArrayList<WorkerBean> workerList = entry.getValue();
            Fragment fragment = new WorkerListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(WorkerListFragment.EXTRA_WORKER_LIST, workerList);
            fragment.setArguments(bundle);
            adapter.addFragment(fragment, key);
        }
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
