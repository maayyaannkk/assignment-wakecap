package com.wakecap.assignment.network;

public interface NetworkResultListener {
    void onPreExecute();

    void onResult(String result);

    void onError(String error);
}
