package com.wakecap.assignment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.wakecap.assignment.adapter.WorkerAdapter
import com.wakecap.assignment.bean.WorkerBean
import com.wakecap.assignment.network.ApiHelper
import com.wakecap.assignment.network.NetworkResultListener
import org.json.JSONObject

class OrdinaryWorkerActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ordinary_worker)

        this.title = "Workers (Ordinary List)"

        recyclerView = findViewById(R.id.recyclerView)
        progressBar = findViewById(R.id.progressBar)

        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        ApiHelper(object : NetworkResultListener {
            override fun onPreExecute() {
                showProgress()
            }

            override fun onResult(result: String?) {
                val workersList = ArrayList<WorkerBean>()

                val workerArray = JSONObject(result).getJSONObject("data").getJSONArray("items")
                for (i in 0..(workerArray.length() - 1)) {
                    val item = workerArray.getJSONObject(i).getJSONObject("attributes")
                    workersList.add(WorkerBean(item.getInt("id"), item.getString("full_name"), item.getString("role")))
                }
                val adapter = WorkerAdapter(workersList, applicationContext)
                recyclerView.adapter = adapter
                hideProgress()
            }

            override fun onError(error: String?) {
                hideProgress()
                Toast.makeText(applicationContext,"Invalid API response",Toast.LENGTH_SHORT).show()
            }
        }).execute()
    }

    private fun showProgress() {
        recyclerView.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        recyclerView.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }
}
