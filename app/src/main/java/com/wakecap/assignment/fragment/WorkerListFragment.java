package com.wakecap.assignment.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.wakecap.assignment.R;
import com.wakecap.assignment.adapter.WorkerAdapter;
import com.wakecap.assignment.bean.WorkerBean;

import java.util.ArrayList;

public class WorkerListFragment extends Fragment {

    public static final String EXTRA_WORKER_LIST = "extra_worker_list";

    RecyclerView recyclerView;
    ProgressBar progressBar;

    public WorkerListFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_ordinary_worker, container, false);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        progressBar = rootView.findViewById(R.id.progressBar);

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        ArrayList<WorkerBean> workerList = (ArrayList<WorkerBean>) getArguments().getSerializable(EXTRA_WORKER_LIST);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        WorkerAdapter workerAdapter = new WorkerAdapter(workerList, getActivity());
        recyclerView.setAdapter(workerAdapter);
        return rootView;
    }
}
