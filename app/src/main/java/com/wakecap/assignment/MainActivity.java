package com.wakecap.assignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonOrdinary, buttonGrouped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOrdinary = findViewById(R.id.buttonOrdinary);
        buttonGrouped = findViewById(R.id.buttonGroup);

        buttonOrdinary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OrdinaryWorkerActivity.class));
            }
        });
        buttonGrouped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GroupedWorkerActivity.class));
            }
        });
    }
}
